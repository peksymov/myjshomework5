alert('Экранирование в языках программирования используется для того, что записать символы, которые являются управляющими в самом языке в виде тексата')
function createNewUSer() {
    const userFirstName = prompt('Enter name', 'Serhii');
    const userLastName = prompt('Enter LastName', 'Peksymov');
    const userDateOfBirth = prompt('Date', '07.08.1985');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        dateOfBirth: userDateOfBirth,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let date = new Date;
            let birthdayDate = Date.parse(userDateOfBirth.split('.').reverse().join('.'));
            return Math.floor((date-birthdayDate)/(24*3600*365*1000));
        },
        getPassword: function () {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.dateOfBirth.substring(6));
        }
    }
    return newUser;
}
const newUser = createNewUSer();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());